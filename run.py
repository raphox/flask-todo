import os
from app import create_app

if __name__ == '__main__':
    app = create_app()

    app.run(
        host  = os.getenv('IP', '0.0.0.0'),
        port  = int(os.getenv('PORT', 8088 )),
        debug = True
    )