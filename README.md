,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 

![Image of Yaktocat](https://codeship.com/projects/42cc3660-d267-0133-eb29-0e34b12277a1/status?branch=master)

Welcome to your Python project on Cloud9 IDE!

To show what Cloud9 can do, we added a basic sample web application to this
workspace, from the excellent Python tutorial _Learning Python the Hard Way_.
We skipped ahead straight to example 50 which teaches how to build a web
application.

If you've never looked at the tutorial or are interested in learning Python,
go check it out. It's a great hands-on way for learning all about programming
in Python.

* _Learning Python The Hard Way_, online version and videos: 
http://learnpythonthehardway.org/book/

* Full book: http://learnpythonthehardway.org

## Starting from the Terminal

To try the example application, type the following in the terminal:

### Gunicorn

```
gunicorn -b 0.0.0.0:8080 app:app
```

### By terminal

```
source venv/bin/activate && python tests.py
```

Alternatively, open the file in ex50/bin and click the green Run
button!

## Create dabase

```
source venv/bin/activate
python
>>> from app import create_app, db                                                                                                                                                                                                           
>>> app = create_app()                                                                                                                                                                                           
>>> app_context = app.app_context()
>>> app_context.push()
>>> db.create_all()
```

## Exceute tests

```
source venv/bin/activate
python tests.py
```

## Configuration

You can configure your Python version and `PYTHONPATH` used in
Cloud9 > Preferences > Project Settings > Language Support.

## Support & Documentation

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE.
To watch some training videos, visit http://www.youtube.com/user/c9ide.