from math import floor

from flask.ext.restplus import Resource, fields
from flask_restplus import reqparse

from app import api

resource_paging = api.model('Paging', {
    'next':       fields.Integer(None),
    'previous':   fields.Integer(None),
    'first_page': fields.Integer(None),
    'last_page':  fields.Integer(None)
})

resource_fields = api.model('Collection', {
    'total':       fields.Integer(None),
    'total_pages': fields.Integer(None),
    'page':        fields.Integer(None),
    'per_page':    fields.Integer(None),
    'paging':      fields.Nested(resource_paging)
})

class Collection(Resource):
    def paginate(self, query):
        parser = reqparse.RequestParser()
        parser.add_argument('page', type=int)
        parser.add_argument('per_page', type=int)

        args = parser.parse_args()

        pagination = query.paginate(page = args.get('page', 1), per_page = args.get('per_page', 20))

        return {
            'items':       pagination.items,
            'total':       pagination.total,
            'total_pages': pagination.pages,
            'page':        pagination.page,
            'per_page':    pagination.per_page,
            'paging': {
                'next':       (pagination.page + 1 if pagination.has_next else None),
                'previous':   (pagination.page - 1 if pagination.has_prev else None),
                'first_page': 1,
                'last_page':  pagination.pages
            }
        }