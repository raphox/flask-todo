from flask.ext.restplus import Api, Resource, fields
from flask_restplus     import reqparse

from app                      import api
from app.models.todo          import Todo as TodoItem
from app.resources.collection import Collection as ResourceCollection
from app.resources.collection import resource_fields as resource_collection_fields

ns = api.namespace('todos', description='TODO operations')

resource_fields = api.model('Todo', {
    'id':    fields.Integer(readOnly=True, description='The todo unique identifier'),
    'title': fields.String(required=True, description='The todo details')
})

resource_collection_fields['items'] = fields.List(fields.Nested(resource_fields))

@ns.route('/')
class TodoCollection(ResourceCollection):
    '''Shows a list of all todos'''

    @ns.doc('list_todos')
    @ns.marshal_with(resource_collection_fields)
    def get(self, **kwargs):
        '''List all tasks'''
        return super(TodoCollection, self).paginate(TodoItem.query)

@ns.route('/<int:id>')
class Todo(Resource):
    '''Show a single todo item, update and delete'''

    @ns.doc('get_todo')
    @ns.marshal_with(resource_fields)
    def get(self, id):
        '''Show a single todo'''
        return TodoItem.query.get(id)