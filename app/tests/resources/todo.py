from flask import json
from flask_restplus import marshal

from app.resources.todo          import resource_fields
from app.tests                   import AppTest
from app.tests.factories.todo    import TodoFactory

class Todo(AppTest):
    
    def test_paginator(self):
        for i in range(0, 25):
            TodoFactory()

        response = self.client.get("/api/v1/todos/?page=2")
        data     = json.loads(response.data)
        
        self.assertStatus(response, 200)

        assert 'items'       in data
        assert 'page'        in data
        assert 'paging'      in data
        assert 'total'       in data
        assert 'total_pages' in data
        
        self.assertEquals(data['page'], 2)
        self.assertEquals(data['total'], 25)
        self.assertEquals(data['total_pages'], 2)
        self.assertEquals(len(data['items']), 5)

    def test_list(self):
        todo  = TodoFactory()
        todo2 = TodoFactory()

        response = self.client.get("/api/v1/todos/")
        data     = json.loads(response.data)
        
        self.assertStatus(response, 200)

        self.assertEquals(data['items'], [
            marshal(todo, resource_fields),
            marshal(todo2, resource_fields),
        ])

    def test_get_by_id(self):
        todo = TodoFactory()

        response = self.client.get("/api/v1/todos/{0}".format(todo.id))
        data     = json.loads(response.data)
        
        self.assertStatus(response, 200)
        self.assertEquals(data, marshal(todo, resource_fields))