import factory
from . import faker

from app.models.todo import Todo as TodoItem
from app             import db

class TodoFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model              = TodoItem
        sqlalchemy_session = db.session

    id    = factory.Sequence(lambda n: n)
    title = factory.LazyAttribute(lambda x: faker.sentence(nb_words=4))