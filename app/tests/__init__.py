from flask.ext.testing import TestCase

from app import create_app, db

class AppTest(TestCase):

    def create_app(self):
        self.app = create_app()

        self.app.config['TESTING']                 = True
        self.app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:////tmp/test.db"
        self.app.config['LIVESERVER_PORT']         = 5000

        self.app_context = self.app.app_context()
        self.app_context.push()

        db.create_all()
        
        return self.app

    def tearDown(self):
        db.session.remove()
        db.drop_all()

        self.app_context.pop()