from flask_restful import fields

from app import db

class Todo(db.Model):
    id    = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False, unique=True)

    def __repr__(self):
        return '<Todo %r>' % self.title