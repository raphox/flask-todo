import os

from flask import Flask, Blueprint, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.cors import CORS

from flask.ext.restplus import Api
from flask.ext.restplus.apidoc import apidoc

bp  = Blueprint('api', __name__, url_prefix = '/api/v1')
api = Api(bp,
    version     = '0.1',
    title       = 'Todo API',
    description = 'A simple Todo API'
)

db = SQLAlchemy()

def create_app():
    app = Flask(__name__, instance_relative_config = True)
    
    # Load the configuration from the instance folder
    app.config.from_pyfile('config.py')
    
    # Load the environment configuration
    env = os.getenv('APP_ENV', 'development')
    app.config.from_object('config.{environment}'.format(environment=env))
    
    @apidoc.add_app_template_global
    def swagger_static(filename):
        return url_for('static', filename = 'bower/swagger-ui/dist/{0}'.format(filename))
    
    app.register_blueprint(bp)
    
    from .resources import todo
    
    # Create database connection object
    db.init_app(app)
    
    cors = CORS(app, resources = { r"/api/*": { "origins": "*" } })
    
    @app.after_request
    def after_request(response):
        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        response.headers.add('Access-Control-Max-Age', '180')
        response.headers.add('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        
        return response
    
    return app