# -*- encoding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name                 = 'Flask Todo',
    version              = '0.1',
    long_description     = __doc__,
    packages             = find_packages(),
    include_package_data = True,
    zip_safe             = False,
    
    # metadata for upload to PyPI
    author       = "Raphael A. Araújo",
    author_email = "raphox.araujo@gmail.com",
    description  = "Management of todos",
    # license      = "",
    keywords     = "flask todo",
    # url          = ""
)